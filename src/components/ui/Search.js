import React, { useState, useRef } from 'react'

const Search = ({ getQuery }) => {
  const [text, setText] = useState('')

  // const inputEl = useRef(null)

  const onChange = (q) => {
    setText(q)
    getQuery(q)
  }

  return (
    <section className='search'>
      <form>
        <input
          // ref={inputEl}
          type='text'
          className='form-control'
          placeholder='Search'
          value={text}
          onChange={(e) => onChange(e.target.value)}
          autoFocus
        />
      </form>

      {/* <button
      onClick={() => {
        console.log(inputEl.current);
      }}
    >
      focus
    </button> */}
    </section>
  )
}

export default Search
