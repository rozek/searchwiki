import React, { useState, useEffect, useRef } from 'react'
import './HighLighter.css'
const reactStringReplace = require('react-string-replace')

const Results = ({ item, query, highLight }) => {
  let strInputCode = item.snippet
  let cleanText = strInputCode.replace(/<\/?[^>]+(>|$)/g, '')
  // let [title, setTitle] = useState(item.title)
  let [titleSmall, setTitleSmall] = useState(item.title)

  let tytuly = item.title

  useEffect(() => {
    // setTitle(tytuly)
    let res = tytuly.toLowerCase()
    setTitleSmall(res)
  }, [query, tytuly])

  const highLighted = {
    color: '#FFA167',
  }

  const content = cleanText.toString()
  const titleContent = titleSmall

  return (
    <>
      <div className='resultContainer'>
        <h3 className='resultTitle'>
          <div>
            <span>
              {reactStringReplace(titleContent, query, (match, i) => (
                <span
                  key={i}
                  style={highLight ? highLighted : { color: '#d3d3d3' }}
                >
                  {match}
                </span>
              ))}
            </span>
          </div>
        </h3>
        <div className='resultContent'>
          <span>
            {reactStringReplace(content, query, (match, i) => (
              <span
                key={i}
                style={highLight ? highLighted : { color: '#d3d3d3' }}
              >
                {match}
              </span>
            ))}
          </span>
        </div>
      </div>
      <hr className='separator'></hr>
      <hr className='separator2'></hr>
    </>
  )
}

export default Results
