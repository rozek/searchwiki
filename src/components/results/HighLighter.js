import React, { useState } from 'react'
import './HighLighter.css'

const HighLighter = ({ query, getHighLight }) => {

    const [text, setText] = useState('');
    const [highLight, setHighLight] = useState(true);

    const highLightAll = (h) => {

        setHighLight(!highLight)
        setText(h)
        getHighLight(h)
        console.log(highLight);
}


    return (
        <div className='compare'>
            <form>
                <input
                 type='text'
                 className='form-control'
                 placeholder='HighLight'
                 value={query}
                //  onChange={(e) => highLightAll(e.target.value)}
                 autoFocus
                 />
            </form>
            <button 
            className='myButton'
            // onClick={(e) => highLightAll(e.target.value)}
            onClick={(e) => highLightAll(highLight)}
            >
            Highlight
            </button>
        </div>
    )
}

export default HighLighter
