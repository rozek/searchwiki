import React, { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios'
import Axios from 'axios';
import Search from "./components/ui/Search";
import Results from "./components/results/Results";
import HighLighter from './components/results/HighLighter'

function App() {

  const [query, setQuery] = useState();
  const [items, setItems] = useState([]);
  const [highLight, setHighLight] = useState('');


  useEffect(() => {

try {
  const fetchItems = async () => {
    
    const result = await axios(`https://en.wikipedia.org/w/api.php?action=query&list=search&format=json&origin=*&srsearch=${query}`)
    // console.log(result.data.query.search);
    const searchResults = result.data.query.search
    setItems(searchResults)
  }

  !query ? console.log('empty') : fetchItems()
} catch (error) {
  console.log(error);
}
  }, [query])
  
console.log(highLight);

  return (
    <div className="container">
      <h2 className='appTitle'> Wikipedia Search Robot </h2>
       <Search getQuery={(q) => setQuery(q)} />

       <HighLighter 
       getHighLight={(h) => setHighLight(h)}
       query= { query }
       ></HighLighter>
      <section className='results'>
        {items && items.map(item => (
          <Results
          item={item}
          query={query}
          key={item.pageid}
          highLight={highLight}
          > 
          </Results>
        ))}

      </section>

    </div>
  );
}

export default App;
